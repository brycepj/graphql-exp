const { graphql, buildSchema } = require('graphql');

const schema = buildSchema(`
  type Query {
    hello: String,
  }
`);

const resolvers = {
  hello() {
    return 'Hello World';
  }
};

graphql(schema, '{ hello }', resolvers).then(response => console.log(response));
